# scripting-js-intermedio

### Los scripts que se han trabajado hasta ahora en el repositorio son:   

1. Suma de numeros en un rango  
* Dado un array con dos números que pueden estar ordenados o no, suma el valor de los números y los números intermedios entre estos.  

2. Comparando arrays  
* Dados dos arrays, los recorremos y los comparamos de forma que retornamos un tercer array con los elementos que no se encuentran en el array comparado.

3. Se añaden los scripts del curso más fácil.