/**
* El método reduce() aplica una función a un acumulador y a cada valor de un array 
* (de izquierda a derecha) para reducirlo a un único valor.
*/

/**Link al manual de mozilla en español con la explicación del método */
// https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/reduce

// SINTAXIS
// var resultado = arr.reduce(funcion[, valorInicial]);

// EJEMPLOS: 

/** 1. Sumar todos los valores de un Array */
var valorInicial = 4;
var total = [0, 1, 2, 3, 4].reduce(function(valorAnterior, valorActual){ return valorAnterior + valorActual; }, valorInicial);
// total == 14
console.log(total);

// EXPLICACIÓN 
/** El método reduce ejecuta la función por cada uno de los elementos
 * del array
 */

/** 2. Integrar un array a partir de varios arrays */
var integrado = [[0,1], [2,3], [4,5]].reduce(function(a,b) {
    return a.concat(b);
  });
// integrado es [0, 1, 2, 3, 4, 5]
console.log(integrado);