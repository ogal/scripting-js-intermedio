/**
 * El método filter() crea un nuevo array con todos los elementos que cumplan 
 * la condición implementada por la función dada.
 */

 // var newArray = arr.filter(callback(currentValue[, index[, array]])[, thisArg])
 /**Link al sitio wen de Mozilla:
  * https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/filter
  */

 var fruits = ['apple', 'banana', 'grapes', 'mango', 'orange'];

/**
 * Filtra la matríz en función de un criterio de búsqueda (query)
 */
// function filtrarElementos ( consulta ) {
//     return fruits.filter(function(el){
//         return el.toLowerCase().indexOf(consulta.toLowerCase()) > -1;
//     })
// }

function filtrarElementos ( consulta ) {
    return fruits.filter = el => {
        return el.toLowerCase().indexOf(consulta.toLowerCase()) > -1;
    }
}

console.log(filtrarElementos('an'));