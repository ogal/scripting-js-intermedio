/** --------------- RESPUESTA DEL INSTRUCTOR UTILIZANDO ES6 ----------------
 * 
 * Intermediate JS Algorithms
 * 1.
 * Sum all numbers in a range 
 * Dado un array con dos números, suma sus cifras y la de los números intermedios, 
 * los números del array no tienen porqué estar en orden. 
 * EX ) [ 3, 1 ] & [ 1, 4 ]
 */

var misNums = [ 3, 1 ];
var misNums2 = [ 1, 4 ];

function sumAll( arr ) {
    var sum = 0;
    // Utilizando los 3 puntos le pasamos el array entero en lugar de una de las posiciones
    for (var i = Math.min(...arr); i <= Math.max(...arr); i++) {
        sum += i; 
    }
    return sum;
}

console.log(sumAll(misNums));
console.log(sumAll(misNums2));