/** --------------- RESPUESTA DEL INSTRUCTOR UTILIZANDO ES6 ----------------
 * 
 * Intermediate JS Algorithms
 * 1.
 * Sum all numbers in a range 
 * Dado un array con dos números, suma sus cifras y la de los números intermedios, 
 * los números del array no tienen porqué estar en orden. 
 * EX ) [ 3, 1 ] & [ 1, 4 ]
 */

var misNums = [ 3, 1 ];
var misNums2 = [ 1, 4 ];

function sumAll( arr ) {
    // Using ES6 arrow function (one-liner)
    var sortedArray = arr.sort((a, b) => a-b);
    var firstNum = arr[0];
    var lastNum = arr[1];

    // Using Arithmetic Progression Summing Formula
    var sum = (lastNum - firstNum + 1) * (firstNum + lastNum) / 2;
    return sum;
}

console.log(sumAll(misNums));
console.log(sumAll(misNums2));