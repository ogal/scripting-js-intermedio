/** -- RESPUESTA DEL INSTRUCTOR --
 * 
 * Intermediate JS Algorithms
 * 1.
 * Sum all numbers in a range 
 * Dado un array con dos números, suma sus cifras y la de los números intermedios, 
 * los números del array no tienen porqué estar en orden. 
 * EX ) [ 3, 1 ] & [ 1, 4 ]
 */
var misNums = [ 3, 1 ];
var misNums2 = [ 1, 4 ];

function sumAll( arr ) {
    // Tomamos el número máximo por si no están en orden
    var max = Math.max( arr[0], arr[1] );
    var min = Math.min( arr[0], arr[1] );
    var temp = 0;
    for (var i = min; i <= max; i++)  {
        temp += i;
    }
    
    return temp;
}

console.log(sumAll(misNums));
console.log(sumAll(misNums2));