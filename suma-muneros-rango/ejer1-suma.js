/** ----------- MI RESPUESTA -------------------
 * 
 * Intermediate JS Algorithms
 * 1.
 * Sum all numbers in a range 
 * Dado un array con dos números, suma sus cifras y la de los números intermedios, 
 * los números del array no tienen porqué estar en orden. 
 * EX ) [ 3, 1 ] & [ 1, 4 ]
 */
var misNums = [ 3, 1 ];
var misNums2 = [ 1, 4 ];

function ordenaNums( array ) {
    // Tomamos el número máximo por si no están en orden
    if (array[1] < array[0]) {
        var pivote = 0;
        pivote = array[0];
        array[0] = array[1];
        array[1] = pivote;
    } 
    return array;
}

function sumaRango( array ) {
    var suma = 0;
    for (var i = array[0]; i <= array[1]; i++) {
        suma += i;
    }
    console.log(suma);
}

 sumaRango( ordenaNums(misNums) );
 sumaRango( ordenaNums(misNums2) );