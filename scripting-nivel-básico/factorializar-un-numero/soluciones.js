/** Factorializar un número
 * Los numeros que se van a factorializar son los: 5, 10, 20, 0
 */

//  1ª Solucion

function factorializar(num){

    var resultado = 1;

    for(var i = num; i > 0; i--){

        resultado *= i;

    }
    return resultado;
}

console.log(factorializar(5));
console.log(factorializar(10));
console.log(factorializar(20));
console.log(factorializar(0));