/**Funcion para multiplicar dos numeros estandar */
function multi(numero) {
    return numero * 2;
}

/**La misma función para multiplicar dos numeros con ES6 */
let multiArrow = params => (params * 2);

console.log(multiArrow(8));

/**Funcion Arrow sin parametros  */
var sinParam = () => num = 2 + 2;

// console.log(sinParam);

/**Con funciones a las que pasamos un unico parametro, los parentesis son opcionales 
 * Con más de un parametro debemos ponerlos.
*/
var unParam = x => x + 3;

console.log(unParam(4));

/**Arrow Functions con statements
 * En este caso debemos de poner llaves, una vez hayan llaves debemos de indicar return
 */
var feedTheCat = (cat) => {
    if (cat === 'hungry') {
      return 'Feed the cat';
    } else {
      return 'Do not feed the cat';
    }
  }
  console.log(feedTheCat("Nope"));

  var addValues = (x, y) => {
      return x + y;
  }

//   var objectLiteral = x =>{{}}
var materials = [
    'Hydrogen',
    'Helium',
    'Lithium',
    'Beryllium'
  ];

var mapeo =  materials.map(key => materials[2]);
console.log(mapeo[3]);

/**  ES5 - Vemos que debemos utilizar el .bind(this) para que tenga efecto dentro de la funcion, 
* sino por defecto nos dará undefined.
*/
var obj = {
    id: 42,
    counter: function counter() {
      setTimeout(function() {
        console.log(this.id);
      }.bind(this), 1000);
    }
  };

// ES6
var obj = {
    id: 42,
    counter: function counter() {
      setTimeout( () => {
        console.log(this.id);
      }, 1000);
    }
  };