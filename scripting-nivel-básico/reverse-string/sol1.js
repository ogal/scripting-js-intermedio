/* Ejercicio consistente en darle la vuelta a una palabra 
* FUNCIONES split() - reverse() - join()
*/
// Ex. Hello 
// Ex. howdy
// E. Hola Mundo 

// 1ª Solución 

function reverseString(palabra){

    palabra = palabra.split('');
    palabra = palabra.reverse('');
    palabra = palabra.join('');

    return palabra;
}

// console.log(reverseString("Hello"));
// console.log(reverseString("howdy"));
// console.log(reverseString("Hola mundo"));

// 2ª Solución 

function reverseString2(palabra){

    var nuevaPalabra = "";

    for( var i = palabra.length -1 ; i >= 0 ; i-- ){
        
        nuevaPalabra += palabra[i];
    }
    return nuevaPalabra;
}

// console.log(reverseString2("Hello"));
// console.log(reverseString2("howdy"));
// console.log(reverseString2("Hola mundo"));

// 3ª Solución 

function reverseString3(palabra){

    return palabra.split('').reverse().join('');
}

console.log(reverseString3("Hello"));
console.log(reverseString3("howdy"));
console.log(reverseString3("Hola mundo"));