/** EJERCICIO 1 - solución intermedia
* Encuentra la palabra más larga en un array, 
* no retornamos la palabra, sino que retornamos la longitud*/ 

var frase1 = "The quick brown fox jumped over the lazy dog";
var frase2 = "What if we try a super-long word such as otorhinolaryngology";

console.log(frase1.split(' '));

function findLonguestWord(str) {
    /**
     * El método reduce() aplica una función a un acumulador y a cada valor 
     * de un array (de izquierda a derecha) para reducirlo a un único valor.
     */
    return str.split(' ').reduce(function(x, y) {
        return Math.max(x, y.length)
    }, 0);
}

console.log(findLonguestWord(frase1));
console.log(findLonguestWord(frase2));