/** EJERCICIO 1 - solución básica
* Encuentra la palabra más larga en un array, 
* no retornamos la palabra, sino que retornamos la longitud*/ 

var frase1 = "The quick brown fox jumped over the lazy dog";
var frase2 = "What if we try a super-long word such as otorhinolaryngology";

function findLonguestWord(str) {

    var palabras = str.split(' ');
    var maxLongitud = 0;
    console.log(palabras);

    for ( let i = 0; i < palabras.length; i++ ) {

        if (palabras[i].length > maxLongitud) {
            maxLongitud = palabras[i].length;
        }
    }

    return "La palabra más larga de la frase tiene: " + maxLongitud + " letras";
}

console.log(findLonguestWord(frase1));
console.log(findLonguestWord(frase2));