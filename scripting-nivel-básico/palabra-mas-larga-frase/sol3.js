/** EJERCICIO 1 - solución intermedia
* Encuentra la palabra más larga en un array, 
* no retornamos la palabra, sino que retornamos la longitud*/ 

var frase1 = "The quick brown fox jumped over the lazy dog";
var frase2 = "What if we try a super-long word such as otorhinolaryngology";

function findLonguestWord(str) {
    str = str.split(' ');

    if (str.length == 1) {
        return str[0].length;
    }

    if(str[0].length >= str[1].length) {
        str.splice(1, 1);
        return findLonguestWord(str.join(' '));
    }

    if(str[0].length <= str[1].length) {
        return findLonguestWord(str.slice(1, str.length).join(' '));
    }
    return str.length;
}

console.log(findLonguestWord(frase1));
console.log(findLonguestWord(frase2));