/** 
* Dividir (truncate) una string dado un numero 
* FUNCION slice()
*/
// Naranja --> 4, 3, 7

// 1ª Solucion

function truncateString(palabra, num){
    
    if(palabra.length > num && num > 3){
        return palabra.slice(0, (num - 3)) + "...";
        
    }else if(palabra.length > num && num <= 3){
        return palabra.slice(0, num) + "...";
        
    }else{
        return palabra;
        
    }
    
}

// console.log(truncateString('Naranja', 4));
// console.log(truncateString('Naranja', 3));
// console.log(truncateString('Naranja', 7));

// 2ª Solucion

function truncateString2(palabra, num){
    
    if(palabra.length > num){
        
        return palabra.slice(0, num > 3 ? num - 3 : num) + '...';
    }
    return palabra;
}

console.log(truncateString2('Naranja', 1));
console.log(truncateString2('Naranja', 3));
console.log(truncateString2('Naranja', 7));