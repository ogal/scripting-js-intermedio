/** --------------- RESPUESTA DEL INSTRUCTOR UTILIZANDO ES6 ----------------
 * 
 * Curso Intermediate JS Algorithms
 * 2.
 * Compara dos arrays  
 * Dados dos arrays, comparalos de forma que se cree un tercero con los numeros del primero 
 * que no encontramos en el segundo y viceversa.
 */
var nums1 = [ 1, 2, 3, 5, 7 ];
var nums2 = [ 1, 2, 3, 4, 5, 8, 9 ];

function diffArray(arr1, arr2) {
    return arr1.concat(arr2).filter( 
        item => !arr1.includes(item) || !arr2.includes(item) 
    );
}

console.log(diffArray(nums1, nums2));