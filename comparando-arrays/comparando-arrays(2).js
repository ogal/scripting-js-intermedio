/** --------------- RESPUESTA DEL INSTRUCTOR UTILIZANDO ES6 ----------------
 * 
 * Curso Intermediate JS Algorithms
 * 2.
 * Compara dos arrays  
 * Dados dos arrays, comparalos de forma que se cree un tercero con los numeros que no coinciden
 * entre el primero y el segundo
 * 
 */
var nums1 = [ 1, 2, 3, 5, 7 ];
var nums2 = [ 1, 2, 3, 4, 5, 8, 9 ];

function diffArray(arr1, arr2) {
    return arr1.filter( 
        // Creamos una variable element, que si no incluye la variable el en array 2
            el => !arr2.includes(el) )
        .concat( arr2.filter( 
            el => !arr1.includes(el) ) );
}

console.log(diffArray(nums1, nums2));