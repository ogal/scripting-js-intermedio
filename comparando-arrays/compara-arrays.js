var nums1 = [ 1, 2, 3, 5, 7 ];
var nums2 = [ 1, 2, 3, 4, 5, 8, 9 ];

function comparaArrays (arr1, arr2) {
    var diferentes = [];

    function onlyInFirst( first, second) {
        for(var i = 0; i < first.length; i++) {

            if (second.indexOf(first[i]) === -1) {
                diferentes.push(first[i]);
            }
        }
    }
    onlyInFirst( arr1, arr2 );
    onlyInFirst( arr2, arr1 ); 

    return diferentes;
}


